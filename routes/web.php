<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::match(['get','post'],'/calculadora', function () {
    return view('calculadora');
});
Route::get('/home/garantias/', function () {
    return view('garantias');
});



Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/home', 'ProyectosController@index');

Route::resource('/home/proyectos', 'ProyectosController');
Route::resource('/home/greales', 'GrealesController');
Route::resource('/home/gpersonales', 'GpersonalesController');
