<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrealesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('greales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('proyectos_id')->nullable();
            $table->foreign('proyectos_id')->references('id')->on('proyectos');
            $table->string('Nombre_garantia');
            $table->string('Descripcion');
            $table->string('Direccion');
            $table->decimal('Valuado', 50, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('greales');
    }
}
