<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGpersonalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gpersonales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('proyectos_id')->nullable();
            $table->foreign('proyectos_id')->references('id')->on('proyectos');
            $table->string('Nombre_garante');
            $table->string('Apellido_garante');
            $table->string('Direccion');
            $table->date('Fecha_nacimiento');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gpersonales');
    }
}
