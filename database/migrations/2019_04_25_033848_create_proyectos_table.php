<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyectos', function (Blueprint $table) {
          $table->bigIncrements('id');
          $table->unsignedInteger('users_id')->nullable();
          $table->foreign('users_id')->references('id')->on('users');
          $table->string('Nombre_proyecto');
          $table->string('Descripcion');
          $table->date('Fecha_inicio');
          $table->date('Fecha_fin');
          $table->decimal('Monto', 50, 2);
          $table->integer('Interes');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyectos');
    }
}
