@extends("../layouts.app")

@section("cabecera")

Editar registros
@endsection

@section("contenido")

<form  method="post" action="/home/greales/{{$greale->id}}">
  <table>
    <tr>
      <td>Nombre de la garantia :</td>
      <td>

  <input type="text" name='Nombre_garantia' value="{{$greale->Nombre_garantia}}">

  {{csrf_field()}}
  <input type="hidden" name="_method" value="PUT">
</td>
</tr>
<tr>
  <td>Descripcion:</td>
  <td><input type="text" name='Descripcion' value="{{$greale->Descripcion}}"></td>
</tr>
<tr>
  <td>Direccion:</td>
  <td><input type="text" name='Direccion' value="{{$greale->Direccion}}"></td>
</tr>
<tr>
  <td>Valor de la propiedad en $:</td>
  <td><input type="text" name='Valuado' value="{{$greale->Valuado}}"></td>
</tr>
<tr align="center">
  <td>
  <input type="submit" name="enviar" value="Actualizar">
</td>
<td>
<input type="reset" name="borrar"  value="Restablecer">
</td>
</tr>
  </table>
</form>



<form  method="post" action="/home/greales{{$greale->id}}">

  {{csrf_field()}}
  <input type="hidden" name="_method" value="DELETE">
  <input type="submit"  value="Eliminar Registro">
</form>


@endsection

@section("pie")



@endsection
