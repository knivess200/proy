
@extends("../layouts.app")

@section("cabecera")

Leer registros
@endsection



@section("contenido")



<table border="1">
  <tr>
    <!--<td>ID</td>-->

    <td>Nombre de la garantia</td>
    <td>Descripcion</td>
    <td>Direccion</td>
    <td>Valor de la propiedad en $</td>
    <td align="center" colspan="2"><a type="button"class="btn btn-xs btn-info pull-right"  href="{{route('greales.create')}}">Crear Garantia Real</a></td>
  </tr>

    @foreach ($greales as $greale)
    <tr>

      <td>   {{$greale->Nombre_garantia}} </td>
      <td>  {{$greale->Descripcion}}    </td>
      <td>  {{$greale->Direccion}}           </td>
      <td>   {{$greale->Valuado}} </td>
      <td><a type="button"class="btn btn-xs btn-info pull-right" href="{{route('greales.edit', $greale->id)}}" >Editar</a></td>


<td>
      <form  method="post" action="greales/{{$greale->id}}" >

        {{csrf_field()}}
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit"  value="Eliminar Registro">

      </form>
</td>
    </tr>
   @endforeach

</table>


{{ $greales->render() }}

@endsection

@section("pie")



@endsection
