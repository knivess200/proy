@extends("../layouts.app")

@section("cabecera")

Editar registros
@endsection

@section("contenido")

<form  method="post" action="/home/proyectos/{{$proyecto->id}}">
  <table>
    <tr>
      <td>Nombre del Proyecto :</td>
      <td>

  <input type="text" name='Nombre_proyecto' value="{{ old('Nombre_proyecto') }} or {{$proyecto->Nombre_proyecto}}">

  {{csrf_field()}}
  <input type="hidden" name="_method" value="PUT">
</td>
</tr>
<tr>
  <td>Descripcion:</td>
  <td><input type="text" name='Descripcion' value="{{ old('Descripcion') }} or {{$proyecto->Descripcion}}"></td>
</tr>
<tr>
  <td>Fecha inicio:</td>
  <td><input type="text" name='Fecha_inicio' value="{{ old('Fecha_inicio') }} or {{$proyecto->Fecha_inicio}}"></td>
</tr>
<tr>
  <td>Fecha fin:</td>
  <td><input type="text" name='Fecha_fin' value="{{ old('Fecha_fin') }} or {{$proyecto->Fecha_fin}}"></td>
</tr>
<tr>
  <td>Monto en Dolares :</td>
  <td><input type="text" name='Monto' value="{{ old('Monto') }} or {{$proyecto->Monto}}"></td>
</tr>
<tr>
  <td>Interes</td>
  <td><input type="text" name='Interes' value="{{ old('Interes') }} or {{$proyecto->Interes}}"></td>
</tr>
<tr align="center">
  <td>
  <input type="submit" name="enviar" value="Actualizar">
</td>
<td>
<input type="reset" name="borrar"  value="Restablecer">
</td>
</tr>
  </table>
</form>



<form  method="post" action="/home/proyectos/{{$proyecto->id}}">

  {{csrf_field()}}
  <input type="hidden" name="_method" value="DELETE">
  <input type="submit"  value="Eliminar Registro">
</form>


@endsection

@section("pie")



@endsection
