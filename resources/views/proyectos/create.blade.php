@extends("../layouts.app")

@section("cabecera")

Crear Proyectos
@endsection

@section("contenido")

<form  method="post"  action="/home/proyectos" target="_self">
  <table>
    <tr>

      <td>
        <input type="hidden" name='users_id' value="{{auth()->user()->id}}">
      </td>
    </tr>
    <tr>

      <td>Nombre del Proyecto:</td>
      <td>

  <input type="text" name='Nombre_proyecto' value="{{ old('Nombre_proyecto') }}">

  {{csrf_field()}}
</td>
</tr>
<tr>
  <td>Descripcion:</td>
  <td><input type="text" name='Descripcion'  value="{{ old('Descripcion') }}"></td>
</tr>
<tr>
  <td>Fecha inicio:</td>
  <td><input type="date" name='Fecha_inicio'  value="{{ old('Fecha_inicio') }}"></td>
</tr>
<tr>
  <td>Fecha fin:</td>
  <td><input type="date" name='Fecha_fin'  value="{{ old('Fecha_fin') }}"></td>
</tr>
<tr>
  <td>Monto en Dolares $:</td>
  <td><input type="text" name='Monto'  value="{{ old('Monto') }}"></td>
</tr>
<tr>
  <td>Interes:</td>
  <td>
  <input type="radio" name = 'Interes' value ="6" />
  6 %<br/>
  <input type="radio" name = 'Interes' value ="12" />
  12 %<br/>
  <input type="radio" name = 'Interes' value ="18" />
  18 %<br/>
  </td>
</tr>
<tr align="center">

<td>
<input type="reset" name="borrar" value="Limpiar">
</td>
<td>
<input type="submit" name="enviar"  value="Siguiente"  >
</td>
</tr>
  </table>

<!--  <script type="text/javascript">
  window.location="/proyectos";
  </script>
</form>
-->

  @if(count($errors)>0)

    @foreach($errors->all() as $error)

      <li>{{$error}}</li>

    @endforeach
  @endif


@endsection

@section("pie")



@endsection
