
@extends("../layouts.app")

@section("cabecera")

Leer registros
@endsection

@section("contenido")


<table border="1">
  <tr>
    <!--<td>ID</td>-->



    <td>Nombre del Proyecto</td>
    <td>Descripcion</td>
    <td>Fecha inicio</td>
    <td>Fecha fin</td>
    <td>Monto en Dolares $</td>
    <td>Interes</td>
    <td colspan="2" align='center'>Opciones</td>


    @foreach ($proyectos as $proyecto)
    <tr>
      <!--<td>   {{$proyecto->users_id}} </td>-->
      <!--<td>{{$proyecto->created_at}}</td>-->

      <td>   {{$proyecto->Nombre_proyecto}} </td>
      <td>  {{$proyecto->Descripcion}}    </td>
      <td>  {{$proyecto->Fecha_inicio}}           </td>
      <td>   {{$proyecto->Fecha_fin}} </td>
      <td>   {{$proyecto->Monto}} </td>
      <td>   {{$proyecto->Interes}} </td>
      <td><a type="button"class="btn btn-xs btn-info pull-right" href="{{route('proyectos.edit', $proyecto->id)}}" >Editar</a></td>


<td>
      <form  method="post" action="proyectos/{{$proyecto->id}}" >

        {{csrf_field()}}
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit"  value="Eliminar Registro">

      </form>
</td>
    </tr>
   @endforeach

</table>
<table>
  <tr>

      <td align="center" colspan="2">

      <a type="button"class="btn btn-xs btn-info pull-right"  href="{{route('proyectos.create')}}">Crear Proyecto</a>
    </td>
  </tr>
</table>


{{ $proyectos->render() }}

@endsection

@section("pie")



@endsection
