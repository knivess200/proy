
@extends("../layouts.app")

@section("cabecera")

Leer registros
@endsection

@section("contenido")


<table border="1">
  <tr>
    <!--<td>ID</td>-->
    <td>Nombre_garante</td>
    <td>Apellido_garante</td>
    <td>Direccion</td>
    <td>Fecha_nacimiento</td>
    <td align="center" colspan="2"><a type="button"class="btn btn-xs btn-info pull-right"  href="{{route('gpersonales.create')}}">Crear Garantia Personal</a></td>
  </tr>

    @foreach ($gpersonales as $gpersonale)
    <tr>

      <td>   {{$gpersonale->Nombre_garante}} </td>
      <td>  {{$gpersonale->Apellido_garante}}    </td>
      <td>  {{$gpersonale->Direccion}}           </td>
      <td>   {{$gpersonale->Fecha_nacimiento}} </td>
      <td><a type="button"class="btn btn-xs btn-info pull-right" href="{{route('gpersonales.edit', $gpersonale->id)}}" >Editar</a></td>


<td>
      <form  method="post" action="proyectos/gpersonales/{{$gpersonale->id}}" >

        {{csrf_field()}}
        <input type="hidden" name="_method" value="DELETE">
        <input type="submit"  value="Eliminar Registro">

      </form>
</td>
    </tr>
   @endforeach

</table>


{{ $gpersonales->render() }}

@endsection

@section("pie")



@endsection
