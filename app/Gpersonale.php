<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Gpersonale extends Model
{
    //
    protected $fillable=["Nombre_garante","Apellido_garante","Direccion","Fecha_nacimiento"];
}
