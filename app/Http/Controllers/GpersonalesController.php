<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Greale;
use App\Gpersonale;
use App\Proyecto;
use App\User;

class GpersonalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         //
         $gpersonales=Gpersonale::orderBy('id','ASC')->paginate(5);
        return view("gpersonales.index",compact("gpersonales"));
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
         //

         return view("gpersonales.create");




     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(Request $request)
     {

         $gpersonale=new Greale;
         //$proyecto->user()->id = $request->users_id;
         $gpersonale->Nombre_garante = $request->Nombre_garante;
         $gpersonale->Apellido_garante = $request->Apellido_garante;
         $gpersonale->Direccion = $request->Direccion;
         $gpersonale->Fecha_nacimiento = $request->Fecha_nacimiento;
         $gpersonale->save();
          return redirect("/home/gpersonales");
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         //
         $gpersonale=Gpersonale::findOrFail($id);
         return view ("gpersonales.show", compact("gpersonale"));
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
         //
         $gpersonale=Gpersonale::findOrFail($id);
         return view ("gpersonales.edit",compact("gpersonale"));
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
         //
         $gpersonale=Gpersonale::findOrFail($id);
       //  $usuario=Contraseña=bcrypt($request->input('Contraseña'));
         $gpersonale->update($request->all());
         return redirect("/home/gpersonales");
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         //
         $gpersonale=Gpersonale::findOrFail($id);
         $gpersonale->delete();
         return redirect("/home/gpersonales");
     }
}
