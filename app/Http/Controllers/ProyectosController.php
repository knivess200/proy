<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Proyecto;
use App\User;
use Carbon\Carbon;
use App\Http\Requests\CreateProyectosRequest;
class ProyectosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         //
        /* $now= Carbon::now();
         $fechaCreacion=Carbon::parse($request->input('$proyecto->created_at'));
           $fechaActual=Carbon::parse($request->input('$now'));
           $fechalimite=$fechaCreacion->diffInDays($fechaActual);
           */
         $proyectos=Proyecto::orderBy('id','ASC')->paginate(5);


        return view("proyectos.index",compact("proyectos"));
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
         //

         return view("proyectos.create");




     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(CreateProyectosRequest $request)
     {
         //
        /* $request-> validate([

           'Nombre_proyecto'=> 'required|string',
           'Descripcion'=> 'required|string|min:5|max:200',
           'Fecha_inicio'=> 'required|date',
           'Fecha_fin'=> 'required|date',
           'Monto'=> 'required',
         ]);


         */

         $proyecto=new Proyecto;
         //$proyecto->user()->id = $request->users_id;
         $proyecto->users_id = $request->users_id;
         $proyecto->Nombre_proyecto = $request->Nombre_proyecto;
         $proyecto->Descripcion = $request->Descripcion;
         $proyecto->Fecha_inicio = $request->Fecha_inicio;
         $proyecto->Fecha_fin = $request->Fecha_fin;
         $proyecto->Monto = $request->Monto;
         $proyecto->Interes = $request->Interes;
         $proyecto->created_at = $request->created_at;
         $proyecto->save();
          return redirect("/home/garantias");
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         //
         $proyecto=Proyecto::findOrFail($id);
         return view ("proyectos.show", compact("proyecto"));
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
         //
         $proyecto=Proyecto::findOrFail($id);
         return view ("proyectos.edit",compact("proyecto"));
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
         //
         $proyecto=Proyecto::findOrFail($id);
       //  $usuario=Contraseña=bcrypt($request->input('Contraseña'));
         $proyecto->update($request->all());
         return redirect("/home/proyectos");
         //->with('status','el post ha sido actualizado');
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         //
         $proyecto=Proyecto::findOrFail($id);
         $proyecto->delete();
         return redirect("/home/proyectos");
     }
}
