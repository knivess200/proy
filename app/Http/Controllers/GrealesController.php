<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Greale;
use App\Proyecto;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\CreateGrealesRequest;


class GrealesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
     {
         //
         //$resultados = DB::table('proyectos')->where('users_id',auth()->user()->id)->get();
         //return view("greales.index", compact("resultados"));
         $greales=Greale::orderBy('id','ASC')->paginate(5);
         $reales=Proyecto::orderBy('id','ASC');
        return view("greales.index",compact("greales"));
     }

     /**
      * Show the form for creating a new resource.
      *
      * @return \Illuminate\Http\Response
      */
     public function create()
     {
         //
        //$resultado=DB::select('select id from proyectos where users_id =auth()->user()->id ');
        // return view("greales.create",compact("resultado"));
         $resultados = DB::table('proyectos')->where('users_id',auth()->user()->id)->get();
        // $resultado = DB::table('proyectos')->select('select * from proyectos where id = :id', ['id' => 1]);
         return view("greales.create", compact("resultados"));
         //return view("greales.create",['resultado' => $resultado]);


     }

     /**
      * Store a newly created resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @return \Illuminate\Http\Response
      */
     public function store(CreateGrealesRequest $request)
     {

          //$resultado=DB::select('select id from proyectos where user_id ={{auth()->user()->id}} ');
          //$resultado = DB::table('proyectos')->where('users_id',auth()->user()->id)->first();
          //$reales=Proyecto::orderBy('id','ASC');
        //  $reale= new Proyecto;
        //  $reale->proyectos_id = $request->proyectos_id;
        //  $reale->save();

         $greale=new Greale;
         //$proyecto->user()->id = $request->users_id;
         $greale->proyectos_id = $request->proyectos_id;
         $greale->Nombre_garantia = $request->Nombre_garantia;
         $greale->Descripcion = $request->Descripcion;
         $greale->Direccion = $request->Direccion;
         $greale->Valuado = $request->Valuado;
         $greale->save();
          return redirect("/home/greales");
          //return view ("greales.index", compact("resultado"));
          //return view ("greales.index",compact('resultado'));
     }

     /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function show($id)
     {
         //
         $greale=Greale::findOrFail($id);
         return view ("greales.show", compact("greale"));
     }

     /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function edit($id)
     {
         //
         $greale=Greale::findOrFail($id);
         return view ("greales.edit",compact("greale"));
     }

     /**
      * Update the specified resource in storage.
      *
      * @param  \Illuminate\Http\Request  $request
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function update(Request $request, $id)
     {
         //
         $greale=Greale::findOrFail($id);
       //  $usuario=Contraseña=bcrypt($request->input('Contraseña'));
         $greale->update($request->all());
         return redirect("/home/greales");
     }

     /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return \Illuminate\Http\Response
      */
     public function destroy($id)
     {
         //
         $greale=Greale::findOrFail($id);
         $greale->delete();
         return redirect("/home/greales");
     }
}
