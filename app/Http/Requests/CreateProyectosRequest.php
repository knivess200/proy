<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateProyectosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'Nombre_proyecto'=>'required|string|min:5|max:200','Descripcion'=>'required|string|min:5|max:200','Fecha_inicio'=>'required|date','Fecha_fin'=>'required','Monto'=>'required|numeric|between:0,100000.99','Interes'=>'required|numeric|between:0,20.99'

        ];
    }
}
