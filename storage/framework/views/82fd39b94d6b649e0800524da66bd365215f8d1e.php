<?php $__env->startSection("cabecera"); ?>

Leer registros
<?php $__env->stopSection(); ?>

<?php $__env->startSection("contenido"); ?>


<table border="1">
  <tr>
    <!--<td>ID</td>-->
    <td>Nombre_garante</td>
    <td>Apellido_garante</td>
    <td>Direccion</td>
    <td>Fecha_nacimiento</td>
    <td align="center" colspan="2"><a type="button"class="btn btn-xs btn-info pull-right"  href="<?php echo e(route('gpersonales.create')); ?>">Crear Garantia Personal</a></td>
  </tr>

    <?php $__currentLoopData = $gpersonales; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gpersonale): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>

      <td>   <?php echo e($gpersonale->Nombre_garante); ?> </td>
      <td>  <?php echo e($gpersonale->Apellido_garante); ?>    </td>
      <td>  <?php echo e($gpersonale->Direccion); ?>           </td>
      <td>   <?php echo e($gpersonale->Fecha_nacimiento); ?> </td>
      <td><a type="button"class="btn btn-xs btn-info pull-right" href="<?php echo e(route('gpersonales.edit', $gpersonale->id)); ?>" >Editar</a></td>


<td>
      <form  method="post" action="proyectos/gpersonales/<?php echo e($gpersonale->id); ?>" >

        <?php echo e(csrf_field()); ?>

        <input type="hidden" name="_method" value="DELETE">
        <input type="submit"  value="Eliminar Registro">

      </form>
</td>
    </tr>
   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</table>


<?php echo e($gpersonales->render()); ?>


<?php $__env->stopSection(); ?>

<?php $__env->startSection("pie"); ?>



<?php $__env->stopSection(); ?>

<?php echo $__env->make("../layouts.app", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/proyecto/resources/views/gpersonales/index.blade.php ENDPATH**/ ?>