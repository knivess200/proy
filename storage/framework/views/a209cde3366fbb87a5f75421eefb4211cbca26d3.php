<?php $__env->startSection("cabecera"); ?>

Leer registros
<?php $__env->stopSection(); ?>

<?php $__env->startSection("contenido"); ?>


<table border="1">
  <tr>
    <!--<td>ID</td>-->



    <td>Nombre del Proyecto</td>
    <td>Descripcion</td>
    <td>Fecha inicio</td>
    <td>Fecha fin</td>
    <td>Monto en Dolares $</td>
    <td>Interes</td>
    <td colspan="2" align='center'>Opciones</td>


    <?php $__currentLoopData = $proyectos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $proyecto): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <tr>
      <!--<td>   <?php echo e($proyecto->users_id); ?> </td>-->
      <!--<td><?php echo e($proyecto->created_at); ?></td>-->

      <td>   <?php echo e($proyecto->Nombre_proyecto); ?> </td>
      <td>  <?php echo e($proyecto->Descripcion); ?>    </td>
      <td>  <?php echo e($proyecto->Fecha_inicio); ?>           </td>
      <td>   <?php echo e($proyecto->Fecha_fin); ?> </td>
      <td>   <?php echo e($proyecto->Monto); ?> </td>
      <td>   <?php echo e($proyecto->Interes); ?> </td>
      <td><a type="button"class="btn btn-xs btn-info pull-right" href="<?php echo e(route('proyectos.edit', $proyecto->id)); ?>" >Editar</a></td>


<td>
      <form  method="post" action="proyectos/<?php echo e($proyecto->id); ?>" >

        <?php echo e(csrf_field()); ?>

        <input type="hidden" name="_method" value="DELETE">
        <input type="submit"  value="Eliminar Registro">

      </form>
</td>
    </tr>
   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

</table>
<table>
  <tr>

      <td align="center" colspan="2">

      <a type="button"class="btn btn-xs btn-info pull-right"  href="<?php echo e(route('proyectos.create')); ?>">Crear Proyecto</a>
    </td>
  </tr>
</table>


<?php echo e($proyectos->render()); ?>


<?php $__env->stopSection(); ?>

<?php $__env->startSection("pie"); ?>



<?php $__env->stopSection(); ?>

<?php echo $__env->make("../layouts.app", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/proyecto/resources/views/proyectos/index.blade.php ENDPATH**/ ?>