<?php $__env->startSection("cabecera"); ?>

Crear Proyectos
<?php $__env->stopSection(); ?>

<?php $__env->startSection("contenido"); ?>

<form  method="post"  action="/home/proyectos" target="_self">
  <table>
    <tr>

      <td>
        <input type="hidden" name='users_id' value="<?php echo e(auth()->user()->id); ?>">
      </td>
    </tr>
    <tr>

      <td>Nombre del Proyecto:</td>
      <td>

  <input type="text" name='Nombre_proyecto' value="<?php echo e(old('Nombre_proyecto')); ?>">

  <?php echo e(csrf_field()); ?>

</td>
</tr>
<tr>
  <td>Descripcion:</td>
  <td><input type="text" name='Descripcion'  value="<?php echo e(old('Descripcion')); ?>"></td>
</tr>
<tr>
  <td>Fecha inicio:</td>
  <td><input type="date" name='Fecha_inicio'  value="<?php echo e(old('Fecha_inicio')); ?>"></td>
</tr>
<tr>
  <td>Fecha fin:</td>
  <td><input type="date" name='Fecha_fin'  value="<?php echo e(old('Fecha_fin')); ?>"></td>
</tr>
<tr>
  <td>Monto en Dolares $:</td>
  <td><input type="text" name='Monto'  value="<?php echo e(old('Monto')); ?>"></td>
</tr>
<tr>
  <td>Interes:</td>
  <td>
  <input type="radio" name = 'Interes' value ="6" />
  6 %<br/>
  <input type="radio" name = 'Interes' value ="12" />
  12 %<br/>
  <input type="radio" name = 'Interes' value ="18" />
  18 %<br/>
  </td>
</tr>
<tr align="center">

<td>
<input type="reset" name="borrar" value="Limpiar">
</td>
<td>
<input type="submit" name="enviar"  value="Siguiente"  >
</td>
</tr>
  </table>

<!--  <script type="text/javascript">
  window.location="/proyectos";
  </script>
</form>
-->

  <?php if(count($errors)>0): ?>

    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

      <li><?php echo e($error); ?></li>

    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>


<?php $__env->stopSection(); ?>

<?php $__env->startSection("pie"); ?>



<?php $__env->stopSection(); ?>

<?php echo $__env->make("../layouts.app", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/proyecto/resources/views/proyectos/create.blade.php ENDPATH**/ ?>