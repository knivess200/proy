<?php $__env->startSection("cabecera"); ?>

Editar registros
<?php $__env->stopSection(); ?>

<?php $__env->startSection("contenido"); ?>

<form  method="post" action="/home/proyectos/<?php echo e($proyecto->id); ?>">
  <table>
    <tr>
      <td>Nombre del Proyecto :</td>
      <td>

  <input type="text" name='Nombre_proyecto' value="<?php echo e($proyecto->Nombre_proyecto); ?>">

  <?php echo e(csrf_field()); ?>

  <input type="hidden" name="_method" value="PUT">
</td>
</tr>
<tr>
  <td>Descripcion:</td>
  <td><input type="text" name='Descripcion' value="<?php echo e($proyecto->Descripcion); ?>"></td>
</tr>
<tr>
  <td>Fecha inicio:</td>
  <td><input type="text" name='Fecha_inicio' value="<?php echo e($proyecto->Fecha_inicio); ?>"></td>
</tr>
<tr>
  <td>Fecha fin:</td>
  <td><input type="text" name='Fecha_fin' value="<?php echo e($proyecto->Fecha_fin); ?>"></td>
</tr>
<tr>
  <td>Monto en Dolares :</td>
  <td><input type="text" name='Monto' value="<?php echo e($proyecto->Monto); ?>"></td>
</tr>
<tr align="center">
  <td>
  <input type="submit" name="enviar" value="Actualizar">
</td>
<td>
<input type="reset" name="borrar"  value="Restablecer">
</td>
</tr>
  </table>
</form>



<form  method="post" action="/home/proyectos/<?php echo e($proyecto->id); ?>">

  <?php echo e(csrf_field()); ?>

  <input type="hidden" name="_method" value="DELETE">
  <input type="submit"  value="Eliminar Registro">
</form>


<?php $__env->stopSection(); ?>

<?php $__env->startSection("pie"); ?>



<?php $__env->stopSection(); ?>

<?php echo $__env->make("../layouts.app", \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/proyecto/resources/views/proyectos/edit.blade.php ENDPATH**/ ?>