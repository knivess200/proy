
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>

    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">

  <style media="screen">
  .contenido form, table{
width: auto;
margin:0 auto;

  }
  </style>
</head>
  <body>
    <table align="center" border="1" WIDTH="50%" >
<tr>


        <td>
            <img src="/images/greal.png" alt align="center"  WIDTH="400"
	  HEIGHT="300">

        </td>


        <td>
          <img src="/images/gpersonal.png" alt="" align="center"  WIDTH="400"
	  HEIGHT="300">
        </td>
        </tr>
        <tr align="center">
          <td><a type="button"  class="btn btn-xs btn-info pull-right" href="/home/greales" >Garantias Reales</a></td>
          <td><a type="button"  class="btn btn-xs btn-info pull-right" href="/home/gpersonales" >Garantias Personales</a></td>


        </tr>
        <tr>
          <td WIDTH="300">Una garantía real (a diferencia de una garantía personal) es un contrato o negocio jurídico accesorio que liga inmediata y directamente al acreedor con la cosa especialmente sujeta al cumplimiento de una determinada obligación principal. Cumplidos los requisitos constitutivos, la garantía real es por sí misma un derecho real.

En los supuestos de garantía real el acreedor está investido de un poder especial sobre la cosa que asegura su derecho, que engloba la llamada reipersecutoriedad, que supone un poder especial de restitución independiente de los sujetos y situaciones en que la cosa gravada pudiera encontrarse. La garantía real permite al acreedor propiciar la venta forzosa del bien gravado, para su realización y pago de la deuda garantizada.
        </td>
        <td WIDTH="300">
Se denominan garantías personales a aquellas formas de amparar el cumplimiento obligacional, donde una o más personas se presentan para responder conjunta o solidariamente con el deudor por el pago de la deuda. Se denominan fiadores o avalistas, y las instituciones que crean estas figuras jurídicas, no son garantías reales, pues no hay ningún bien específico destinado a ser ejecutado en caso de que el deudor no cumpla con lo que se obligó, sino que es todo el patrimonio del garante, el afectado.
        </td>
        </tr>


    </table>

  </body>
</html>
<?php /**PATH /var/www/html/proyecto/resources/views/garantias.blade.php ENDPATH**/ ?>